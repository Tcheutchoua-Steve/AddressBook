/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dev.mars.addressbook.util;

/**
 *
 * @author Gil
 */

// Restricting a gender to be either male or female 
public enum ContactGender {
    MALE , FEMALE 
}
